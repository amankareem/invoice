/**
 * @author AmanKareem <aman.kareem@storeking.in>
 * @since March 24 2018;
 * @fileOverview Based on: Placebo Effect , Butterfly effect;
 */
"use strict;"
var Mongoose = require("mongoose");
Mongoose.Promise = global.Promise;
var http = require("http");
var cuti = require("cuti");
var log4js = cuti.logger.getLogger;
var logger = log4js.getLogger("oms");
var async = require("async");
var _ = require("lodash");

const omsCollection = "omsMaster";
const types = ["unreserve", "reserve", "release", "noChange"];
const onOrderReservationChange_endPoint = "/webhook/onReservationChange";
const onReleaseWebhook_endPoint = "/webhook/onRelease";

/* Parent constructor patter; */
function Invoice(scannedEntity) {
    this._scannedEntity = scannedEntity;
}

function Helper() {
    //Abstraction pattern constructor;
}

/* This will attain equal balance in case of stock Release and hold; */
function Equlibrium() {
    //Abstraction;
}

Invoice.prototype = {
    release: function () {
        logger.trace("RELEASE: Releasing stock on invoicing.....");
        var helper = new Helper();
        var scannedMatrixList = helper._prepareScannedEntityMatrixList(this._scannedEntity);
        var ledgerList = [];
        scannedMatrixList.map(scannedMatrix => {
            var ledger = {
                "snapShotId": scannedMatrix.snapShotId,
                "warehouseId": "WMF0",
                "productId": scannedMatrix.productId,
                "reference": { "subOrderId": scannedMatrix.subOrderId, "performaId": this._scannedEntity.performaInvoice },
                "referenceType": "Release",
                "requestQty": scannedMatrix.quantity,
                "serialNo": scannedMatrix.serialNo
            };
            ledgerList.push(ledger);
        });
        return helper._stockVariant(ledgerList, "oms", onReleaseWebhook_endPoint);
    },
    /* This will execute release and hold logic based on scanned data */
    cycleStocks: function () {
        return new Promise((resolve, reject) => {
            var reserveStockList = [];//stock needed to be put on hold;Also this stock list needs to be checked against other reserved orders;
            var unReserveStockList = [];//stock needed to be removed from hold;Also this needs to be alloted to any other order;
            var releaseStockList = [];//stock where onHold count should be decreased for invoicing;Obtain this after comparo from subOrders considering wh with qty > 0;
            //find orders from subOrderids in scanned payload;
            //convert found order into matrix;
            //convert scanned data into matrix;
            //extract wh ids from order snapshots and scanned payload;
            /* 
                - compare order matrix and scanned matrix;
                - split into reservestocklist , unreservestock list , release stock list;
                - for reserve stock list find dependent or affected orders - as they might of holding sme stock and if we take from hold then there reservation will get effected;
                - for unreserve stock list , do election again to allot the unreserved order;
                - after wh gives response, update order snapshots , batch,invoice
             */
            var helper = new Helper();
            var equilibria = new Equlibrium();
            var orderId = this._scannedEntity.orderId;
            var performaInvoiceNo = this._scannedEntity.performaInvoice;
            var scannedSubOrderIds = this._scannedEntity.subOrderList.map(sO => sO._id);
            //Find one order which is been invoiced;
            helper._findOrder(orderId, performaInvoiceNo, null).then(_order => {//THis uses find one;
                if (!_order) {
                    reject(new Error(`Already invoiced.`));
                    return;
                }
                var orderMatrixList = helper._prepareOrderMatrixList(_order, scannedSubOrderIds);//Prepare order matrix;Unique in terms of - subOrder , productId , whId;
                var scannedMatrixList = helper._prepareScannedEntityMatrixList(this._scannedEntity);//Prepare scanned Matrix;Unique in terms of - subOrder , productId , whId;
                var snapShotIdList = helper._getSnapShotIds(orderMatrixList, scannedMatrixList);//gets unique list of snapshot ids from both matrices;
                //Now compare what was reserved for this current order and what has been scanned;Then divide into appropriate lists;
                orderMatrixList = helper._compareAllotedAndScanned(orderMatrixList, scannedMatrixList);
                //Now divide the order matrix into two lists ; reserveList , unReserveList;
                var groupedOrderMatrixList = helper._segregateList(orderMatrixList);//contains grouped by type orderMatrix list
                reserveStockList = groupedOrderMatrixList["reserve"];//List for adding to hold;
                unReserveStockList = groupedOrderMatrixList["unreserve"];//List to remove quantities from hold;
                /*
                    -  reserveStockList and  unReserveStockList will be empty in case where alloted stock for the order and scanned stocks are identical;
                    -  If scanned stock is different from previously alloted stocks to the order , then in this case  reserveStockList and unReserveStockList must exist and both cannot be empty;
                */
                if (reserveStockList && reserveStockList.length && unReserveStockList && unReserveStockList.length) {
                    logger.trace("Executing Release and Hold counter balance function, as alloted stocks to the order is different from what has been scanned at invoicing");
                    /* This will strike balance so that net effect is the same and return reservedList and unreservedList */
                    equilibria._counterBalance(orderMatrixList, reserveStockList, unReserveStockList, snapShotIdList)
                        .then(result => resolve(result))
                        .catch(e => reject(e));
                } else {
                    logger.trace("Skipping Release and hold logic , scanned and reserved snapshots are same.");
                    resolve();
                }
                //var combinedLedgerList = reserveLedgerList.concat(unreserveLedgerList);
                // helper._stockVariant(combinedLedgerList).then(result => resolve(result)).catch(e => reject(e));
            }).catch(e => reject(e));
        });
    }
}

Helper.prototype = {
    /**
     * @description Common Http request making function block;
     * @param {*String} _magickey //Redis-Key;
     * @param {*String} _path //API Path;
     * @param {*String} _method // Http method - POST , PUT , GET;
     * @param {*Object} _payload //Requset body;
    */
    _fireHTTP: function (_magickey, _path, _method, _payload) {
        return new Promise((resolve, reject) => {
            if (!_magickey) {
                reject(new Error(`Magic Key cannot be empty for HTTP request.`));
                return;
            }
            if (!_path) {
                reject(`Path cannot be empty for HTTP request.`);
                return;
            }
            if (!_method) {
                reject(`Http Method cannot be empty for HTTP request.`);
                return;
            }
            cuti.request.getUrlandMagicKey(_magickey)
                .then(options => {
                    options.path += _path;
                    options.method = _method;
                    var request = http.request(options, response => {
                        var data = "";
                        response.on('data', _data => data += _data.toString());
                        response.on('end', () => {
                            try {
                                data = JSON.parse(data);
                                resolve(data);
                            } catch (e) {
                                reject(e);
                            }
                        });
                    });
                    if ((_method === 'POST' || _method === 'PUT') && !_.isEmpty(_payload))
                        request.end(JSON.stringify(_payload));
                    else
                        request.end();

                }).catch(e => reject(e));
        });
    },
    _findOrder: function (orderId, performaInvoiceNo, callback) {
        if (callback) {
            Mongoose.models['omsMaster'].findOne({
                "_id": orderId,
                "subOrders": {
                    "$elemMatch":
                        {
                            "performaInvoiceNo": performaInvoiceNo,
                            "internalStatus": "BatchEnabled",
                            "status": "Processing",
                            "invoiced": false
                        }
                }
            }).lean().exec()
                .then(_order => callback(null, _order))
                .catch(e => callback(e));
        } else {
            return Mongoose.models['omsMaster'].findOne({
                "_id": orderId,
                "subOrders": {
                    "$elemMatch":
                        {
                            "performaInvoiceNo": performaInvoiceNo,
                            "internalStatus": "BatchEnabled",
                            "status": "Processing",
                            "invoiced": false
                        }
                }
            }).lean().exec();
        }
    },
    _findSnapShots: function (whIds, select, callback) {
        var path = "/snapshot?filter=" + encodeURIComponent(JSON.stringify({ "_id": { "$in": whIds } }));
        if (select && select.length) path += "&select=" + select;
        if (callback) {
            this._fireHTTP("wh", path, "GET", null).then(_snapShots => callback(null, _snapShots)).catch(e => callback(e));
        } else {
            return this._fireHTTP("wh", path, "GET", null);
        }
    },
    _extractOrderSnapShots: function (_order, _scannedSubOrderIds) {
        var allotedSnapShots = [];
        _order.subOrders.map(subOrder => {
            if (_scannedSubOrderIds.indexOf(subOrder._id) > -1) {
                allotedSnapShots = allotedSnapShots.concat(subOrder.snapshots);
            }
        });
    },
    _prepareOrderMatrixList: function (_order, _scannedSubOrderIds) {
        var orderMatrixList = [];//Is unique by suborderId ,productId and warehouse Id;
        //get only scanned suborders;
        var selectedSubOrders = _order.subOrders.filter(sO => _scannedSubOrderIds.indexOf(sO._id) > -1 ? true : false);//get only scanned subOrders from the order;
        //Iterate suborders and snapshots;
        selectedSubOrders.map(subOrder => _.each(subOrder.snapshots, _snapShot => {
            //find record from matrix;
            var matrix = _.find(orderMatrixList, { "subOrderId": subOrder._id, "productId": _snapShot.productId, "snapShotId": _snapShot.snapShotId });
            if (!matrix) {
                var productQty = _.find(subOrder.requestedProducts, { "productId": _snapShot.productId }).quantity;
                var newMatrix = {
                    "subOrderId": subOrder._id,
                    "productId": _snapShot.productId,
                    "snapShotId": _snapShot.snapShotId,
                    "performaInvoiceNo": subOrder.performaInvoiceNo,
                    "batchId": subOrder.batchId,
                    "productQty": productQty,
                    "inventoryQty": _snapShot.quantity,
                    "inventoryKeeper": { "allotedQty": _snapShot.quantity, "changedQty": _snapShot.quantity, "differentialQty": 0, "type": "noChange" }//types enum = "unreserve" , "reserve" , "release","noChange"
                };
                orderMatrixList.push(newMatrix);
            } else {
                //update the existing one;
                matrix.inventoryQty += _snapShot.quantity;
                matrix.inventoryKeeper.allotedQty += _snapShot.quantity;
                matrix.inventoryKeeper.changedQty += _snapShot.quantity;
            }
        }));
        return orderMatrixList;//This array is unique in terms of: suborder,productId and whId;
    },
    _prepareScannedEntityMatrixList: function (_scannedEntity) {
        var scannedMatrixList = [];//This array matrix is unique in terms of: subOrderId , productId and whId;
        //Scanned entity is an object;It has list of subOrders , each suborder will have of list of products scanned ;
        _scannedEntity.subOrderList.map(subOrder => _.each(subOrder.scan, scannedItem => {
            var whId = scannedItem.inventoryids[0];
            var matrix = _.find(scannedMatrixList, { "subOrderId": subOrder._id, "productId": scannedItem.productId, "snapShotId": whId });
            if (!matrix) {
                //insert new record;
                var newMatrix = {
                    "subOrderId": subOrder._id,
                    "productId": scannedItem.productId,
                    "snapShotId": whId,
                    "quantity": scannedItem.quantity,
                    "serialNo": scannedItem.serialNo && scannedItem.serialNo.length ? scannedItem.serialNo : []
                };
                scannedMatrixList.push(newMatrix);
            } else {
                //update the existing record;
                matrix.quantity += scannedItem.quantity;
                matrix.serialNo = matrix.serialNo && matrix.serialNo.length ? matrix.serialNo.concat(scannedItem.serialNo) : [];
            }
        }));
        return scannedMatrixList;
    },
    _compareAllotedAndScanned: function (_orderMatrixList, _scannedMatrixList) {
        /* 
            - compare what was alloted/reserved when order was placed with currently scanned data;
            - classify into stocks to be newly reserved and stocks to be newly unreserved;
            Two steps to iterate:
                1. Iterate orderMatrix and check if alloted snapshot is scanned , if missing eliminate;
                2. Iterate scanned Matrix and if variations found in previously alloted and currently scanned snapshots , then establish the equilibrium;
         */
        //1.Iterate orderMatrix and check if alloted snapshot is scanned , if missing eliminate otherwise leave as is;
        _orderMatrixList.map(_orderMatrix => {
            /*  - For this suborder with THIS product id and whId was reserved at order placement, now check with same suborderId , productid and whId if it is been scanned while invoicing , if not then eliminate i,e remove from hold what was reserved;*/
            var scannedMatrix = _.find(_scannedMatrixList, { "subOrderId": _orderMatrix.subOrderId, "productId": _orderMatrix.productId, "snapShotId": _orderMatrix.snapShotId });
            if (!scannedMatrix) {
                //If not found then eliminate; i,e unreserve/remove from hold; as this was alloted at orderplacement time; but now scanned stock is different so replace;
                _orderMatrix.inventoryKeeper.differentialQty = _orderMatrix.inventoryKeeper.allotedQty;
                _orderMatrix.inventoryKeeper.changedQty = _orderMatrix.inventoryKeeper.allotedQty;//all aloted stock qty should be removed from hold;
                _orderMatrix.inventoryKeeper.type = "unreserve";
            }
        });//order matrix loop ends here;

        // 2. Iterate scanned Matrix and if variations found in previously alloted and currently scanned snapshots , then establish the equilibrium;
        _scannedMatrixList.map(scannedMatrix => {
            // here with scanned data i,e - subOrderId , product id and whId ; order matrix may have or may not have an entry;
            var orderMatrix = _.find(_orderMatrixList, { "subOrderId": scannedMatrix.subOrderId, "productId": scannedMatrix.productId, "snapShotId": scannedMatrix.snapShotId });
            if (orderMatrix) {
                //If found compare what was alloted and what is currently scanned;
                var allotedQty = orderMatrix.inventoryKeeper.allotedQty;//previously alloted qty;
                var scannedQty = scannedMatrix.quantity;//currently scanned qty;
                var differential = this._getDifferentialCount(allotedQty, scannedQty);
                orderMatrix.inventoryKeeper.differentialQty = differential.netQty;//netQty will be positive;
                orderMatrix.inventoryKeeper.type = differential.type;//based on type its determined that whether to reserve,unreserve or nochange;
                orderMatrix.inventoryKeeper.changedQty += differential.count;//count can be positive or negative;
            } else {//New entry which was not alloted before;
                // Allot what is currently scanned and what was previously scanned is already taken care in step.1;
                var subOrder = { "_id": scannedMatrix.subOrderId };
                var snapShot = { "snapShotId": scannedMatrix.snapShotId, "productId": scannedMatrix.productId, "quantity": scannedMatrix.quantity };
                var prodQty = _.find(_orderMatrixList, { "subOrderId": scannedMatrix.subOrderId, "productId": scannedMatrix.productId }).productQty;
                var productQty = prodQty ? prodQty : 0;
                var newOrderMatrix = this._newOrderMatrix(subOrder, snapShot, productQty);
                newOrderMatrix.inventoryKeeper.differentialQty = scannedMatrix.quantity;
                newOrderMatrix.inventoryKeeper.type = "reserve";
                _orderMatrixList.push(newOrderMatrix);// add to orderMatrixList;
            }
        });
        return _orderMatrixList;
    },
    _getSnapShotIds: function (_orderMatrixList, _scannedMatrixList) {
        var snapShotIds = [];
        var orderSnapShotIdList = _orderMatrixList.map(matrix => matrix.snapShotId);//This may or may not be unique , as whId can be used multiple times;
        var scannedSnapShotIdList = _scannedMatrixList.map(scannedMatrix => scannedMatrix.snapShotId);
        snapShotIds = orderSnapShotIdList.concat(scannedSnapShotIdList);
        snapShotIds = _.uniq(snapShotIds);
        return snapShotIds;
    },
    /* subOrder = {"_id" : "ORD100_1"} ; _snapShot = {"snapShotId": "WH100" , "productId" : "PR10001" , "quantity" : 2} ; productQty = 4 */
    _newOrderMatrix: function (subOrder, _snapShot, productQty) {
        var newRecord = {
            "subOrderId": subOrder._id,
            "productId": _snapShot.productId,
            "snapShotId": _snapShot.snapShotId,
            "productQty": productQty,
            "inventoryQty": _snapShot.quantity,
            "inventoryKeeper": { "allotedQty": _snapShot.quantity, "changedQty": _snapShot.quantity, "differentialQty": 0, "type": "noChange" }//types enum = "unreserve" , "reserve" , "release"
        };
        return newRecord;
    },
    _getDifferentialCount: function (allotedQty, scannedQty) {
        const RESERVE_ENUM = "reserve";
        const UNRESERVE_ENUM = "unreserve";
        const NO_CHANGE_ENUM = "noChange";

        var count = scannedQty - allotedQty; //This can yield either negative or positive result;
        /* 
            #.Negative count: (scannedQty < allotedQty) - unreserve
                - If count is negative ; It indicates scanned qty is less than compared to what was alloted to the suborder;
                - Hence alloted is more ; unreserve by netQty value from what was alloted;
            #.Positive count: (scannedQty > allotedQty) - reserve
                - If count is positive , it indicates scanned qty is more than compared to what was alloted to the subOrder;
                - Hence alloted is less ; need to reserve by netQty;
            #.Zero count: (scannedQty === allotedQty)
                - This indicates that what is scanned same qty has been alloted to the subOrder;
                - Hence noChange;
         */

        var result = { "count": 0, "netQty": 0, "type": "" };//reserve , unreserve , noChange;

        if (count === 0) {//no change ; neither reserve nor unreserve;
            result.count = count;//obtained difference;
            result.netQty = 0;
            result.type = "noChange";
        } else if (count < 0) {//negative count; unreserve stock;
            result.count = count;
            result.netQty = count * (-1);//make it positive and assign;
            result.type = "unreserve";
        } else if (count > 0) {//positive count; reserve stock
            result.count = count;
            result.netQty = count;
            result.type = "reserve";
        }

        return result;
    },
    _segregateList: function (_orderMatrixList) {
        return _.groupBy(_orderMatrixList, 'inventoryKeeper.type');
    },
    /* Formats matrix list into ledger request list */
    _formatAsLedgerList: function (_list) {
        //"Invoice reserved", "Invoice unreserved";
        var formattedList = []
        _.each(_list, orderMatrix => {
            var type = "";
            if (orderMatrix.inventoryKeeper.type === 'reserve')
                type = "Invoice reserved";
            else if (orderMatrix.inventoryKeeper.type === 'unreserve')
                type = "Invoice unreserved";

            var ledger = {
                "snapShotId": orderMatrix.snapShotId,
                "warehouseId": "WMF0",
                "productId": orderMatrix.productId,
                "reference": { "subOrderId": orderMatrix.subOrderId, "performaId": orderMatrix.performaInvoiceNo, "batchId": orderMatrix.batchId },
                "requestQty": orderMatrix.inventoryKeeper.differentialQty,
                "referenceType": type
            };
            formattedList.push(ledger);
        })
        return formattedList;
    },
    _stockVariant: function (ledgerList, magicKey, webHook) {
        return new Promise((resolve, reject) => {
            if (!ledgerList || !ledgerList.length) {
                reject(new Error(`Cannot request stock variation, ledger list is empty`));
                return;
            }
            var payload = {
                "list": ledgerList,
                "webhook": {
                    "magicKey": magicKey,
                    "path": webHook//"/webhook/updateOrders"
                }
            };
            this._fireHTTP("wh", '/stockledger/bulkStockVariation', 'PUT', payload).then(result => resolve(result)).catch(e => reject(e));
        });
    },
    _releaseStock: function (_stockPile) {
        //ledger list as param to decrease hold quantity;
    }
}

/* The Placebo Effect :
    - What goes around comes back around : Whenever stock from current order is getting reserved(added to hold) ,at the same time there is also stock getting unreserved(removed from hold);
    - Hence stocks are not getting lost;
    - Substitute for whats been exchanged with whats been made available;
*/
Equlibrium.prototype = {
    /* This is to make sure amount of qty needed to be reserved additionally is made available from some other orders and substitute with some other */
    _counterBalance: function (_orderMatrixList, _reserveStockPile, _unReserveStockPile, snapShotIdList) {
        logger.trace("Inside conter balance funtion for release and hold stocks....");
        return new Promise((resolve, reject) => {
            var helper = new Helper();
            var reserveLedgerList = helper._formatAsLedgerList(_reserveStockPile);//List which needs qty to be put on hold;
            var unreserveLedgerList = helper._formatAsLedgerList(_unReserveStockPile);//List which will remove qty from hold and adds to qty;
            var pendingSubstitutionList = [];//contains orders from where to remove qty onHold and substitute removed qty with unreserving stocks;
            /*  
                - In order to reserve additional stocks while invoicing , make sure they are either in stock are made available by removing necesary stocks from hold;
                - Hence first push request to stock ledger from list unreservingStock list (unreserveLedgerList + pendingSubstitutionList);
                - Then push to request queue of stock ledger the list which needs to add qty on hold;(Before adding to hold make those stocks available by releasing)
            */
            //New unreserveLedger list with keeper appended;
            var unReserveMatrixList = this._matricizeStockPile(unreserveLedgerList);//Remove from hold;
            //New reserveLedger list with keeper appended;
            var reserveMatrixList = this._matricizeStockPile(reserveLedgerList);//Add to hold;
            //Get inventory List;
            helper._findSnapShots(snapShotIdList, ["_id", "productId", "quantity", "onHold"], null)
                .then(inventoryList => {
                    inventoryMatrixList = this._matricizeInventoryList(inventoryList);
                    this._distributeReleasingStock(unReserveMatrixList, inventoryMatrixList);//distribute only unreserving matrix list on wh;
                    //Now iterate reserveMatrixList and execute substitution;Before adding qty to hold check if qty is available ; if not then release and make available and substitute with some other;
                    reserveMatrixList.map(reserveMatrix => {//ledger list format;
                        if (reserveMatrix.keeper.reservingQty > 0) {
                            var inventory = _.find(inventoryMatrixList, { "_id": reserveMatrix.snapShotId, "productId": reserveMatrix.productId });
                            if (inventory) {
                                //Try and take from available qty;
                                var count = this._getCount(reserveMatrix.keeper.reservingQty, inventory.keeper.availableQty);//pass required qty and available qty;
                                //update reserveMatrix keeper;
                                reserveMatrix.keeper.reservingQty -= count;
                                //Update inventory matrix Keeper;
                                inventory.keeper.availableQty -= count;

                                //If reserving qty is still > 0 then form a request to remove from hold and provide substitute/replacement for the loss to the order which has this as dependency;
                                if (reserveMatrix.keeper.reservingQty > 0 && inventory.keeper.onHoldQty) {
                                    var count = this._getCount(reserveMatrix.keeper.reservingQty, inventory.keeper.onHoldQty);
                                    reserveMatrix.keeper.reservingQty -= count;//since reserving matrix has got count from hold qty; deduct how much it has got;
                                    inventory.keeper.onHoldQty -= count;// similarly deduct how much qty has been taken from Hold qty;
                                    //Find inventory which can be swapped/replaced/substituted as we are taking one invetory stock need to balance it with other equivalent inventry;
                                    //Since onHold qty has be taken, it needs to be subsituted with some other inventory;
                                    var substituteInventory = _.find(inventoryMatrixList, matrix => matrix.productId === reserveMatrix.productId && matrix.keeper.availableQty >= count ? true : false);
                                    if (!substituteInventory) { //This can never be undefined as per the facts;
                                        reject(new Error(`Could not cycle stocks as no substitute inventory found for release and hold.`));
                                        return;
                                    }
                                    substituteInventory.keeper.availableQty -= count; // decrease subsitute matrix qty as its taken and alloted;
                                    var substitution = { "unReserveFrom": {}, "reserveFrom": {} };
                                    substitution["unReserveFrom"] = {
                                        "snapShotId": inventory._id,
                                        "productId": inventory.productId,
                                        "removeQty": count,
                                    }
                                    substitution["reserveFrom"] = {
                                        "snapShotId": substituteInventory._id,
                                        "productId": substituteInventory.productId,
                                        "allotQty": count
                                    };
                                    pendingSubstitutionList.push(substitution);
                                }
                            }
                        }
                    });

                    /* Iterate  pendingSubstitutionList and find orders*/
                    if (pendingSubstitutionList && pendingSubstitutionList.length) {
                        Promise.all(pendingSubstitutionList.map(substitute => {
                            return new Promise((resolve, reject) => {
                                this._findOrderForSubstitution(substitute.unReserveFrom.snapShotId, substitute.unReserveFrom.productId, substitute.unReserveFrom.removeQty)
                                    .then(_order => {
                                        if (_order && _order.length) {
                                            //"Invoice reserved", "Invoice unreserved"
                                            var unReserve = {
                                                "snapShotId": substitute.unReserveFrom.snapShotId,
                                                "warehouseId": "WMF0",
                                                "productId": substitute.unReserveFrom.productId,
                                                "reference": { "subOrderId": _order[0].subOrders._id, "performaId": _order[0].subOrders.performaInvoiceNo, "batchId": _order[0].subOrders.batchId },
                                                "requestQty": substitute.unReserveFrom.removeQty,
                                                "referenceType": "Invoice unreserved"
                                            };
                                            var reserve = {
                                                "snapShotId": substitute.reserveFrom.snapShotId,
                                                "warehouseId": "WMF0",
                                                "productId": substitute.reserveFrom.productId,
                                                "reference": { "subOrderId": _order[0].subOrders._id, "performaId": _order[0].subOrders.performaInvoiceNo, "batchId": _order[0].subOrders.batchId },
                                                "requestQty": substitute.reserveFrom.allotQty,
                                                "referenceType": "Invoice reserved"
                                            };
                                            reserveLedgerList.push(reserve);
                                            unreserveLedgerList.push(unReserve);
                                            resolve();
                                        } else {
                                            reject(new Error(`Order not found for substitution.`));
                                        }
                                        //promise all promise is resolved here;
                                    })
                                    .catch(e => reject(e));
                            });
                        })).then(data => {
                            //first fire unreserve list so that stocks are available to hold;
                            var unReservePromise = helper._stockVariant(unreserveLedgerList, "oms", onOrderReservationChange_endPoint);
                            // Then fire reserve list ;
                            var reservePromise = helper._stockVariant(reserveLedgerList, "oms", onOrderReservationChange_endPoint);
                            Promise.all([unReservePromise, reservePromise]).then(result => resolve({ "reservedList": reserveLedgerList, "unReservedList": unreserveLedgerList })).catch(e => reject(e));
                        }).catch(e => reject(e));
                    } else {
                        var unReservePromise = helper._stockVariant(unreserveLedgerList, "oms", onOrderReservationChange_endPoint);
                        // Then fire reserve list ;
                        var reservePromise = helper._stockVariant(reserveLedgerList, "oms", onOrderReservationChange_endPoint);
                        Promise.all([unReservePromise, reservePromise]).then(result => resolve({ "reservedList": reserveLedgerList, "unReservedList": unreserveLedgerList })).catch(e => reject(e));
                    }
                })
                .catch(e => reject(e));

        });
    },
    _matricizeInventoryList: function (inventoryList) {
        inventoryList.map(inventory => {
            //append keeper;
            inventory.keeper = {
                "availableQty": inventory.quantity,
                "onHoldQty": inventory.onHold,
                "usedQty": 0
            }
        });
        return inventoryList;
    },
    _matricizeStockPile: function (matrixList) {//list is in ledger format;
        var matricizedList = [];
        matrixList.map(ledger => {//ledger format;
            var entity = Object.assign({}, ledger);
            //append keeper;
            entity.keeper = {
                "reservingQty": ledger.referenceType === "Invoice reserved" ? entity.requestQty : 0,//This is incase if the list is of type reserve;
                "unReservingQty": ledger.referenceType === "Invoice unreserved" ? entity.requestQty : 0,//This is incase if the list is of type unreserve;
                "usedQty": 0
            };
            matricizedList.push(entity);
        });
        return matricizedList;
    },
    _distributeReleasingStock: function (unReserveMatrixList, inventoryMatrixList) {
        logger.trace("Distributing releasing stocks .......");
        for (var i = 0; i < unReserveMatrixList.length; i++) {
            var unReserveMatrix = unReserveMatrixList[i];//If this inventory is in unReserve list; then it will be having onHold qty greater than equal to its qty;
            if (unReserveMatrix.keeper.unReservingQty > 0) {
                //Find inventory from inventory list with inventory Id , productId and onHoldqty >= unReserve qty;
                var inventory = _.find(inventoryMatrixList, { "_id": unReserveMatrix.snapShotId, "productId": unReserveMatrix.productId });
                if (inventory && inventory.keeper.onHoldQty >= unReserveMatrix.keeper.unReservingQty) {
                    //Update inventory keeper;
                    inventory.keeper.onHoldQty -= unReserveMatrix.keeper.unReservingQty;//remove from hold;
                    inventory.keeper.usedQty += unReserveMatrix.keeper.unReservingQty;
                    inventory.keeper.availableQty += unReserveMatrix.keeper.unReservingQty; //add to available inventory qty;
                    //Update unReserve Matrix keeper;
                    unReserveMatrix.keeper.unReservingQty = 0;
                    unReserveMatrix.keeper.usedQty += unReserveMatrix.keeper.unReservingQty;
                }
            }
        }
    },
    _getCount: function (required, available) {
        return required <= available ? required : required > available ? available : 0;
    },
    _findOrderForSubstitution: function (snapShotId, productId, count) {
        return Mongoose.models['omsMaster'].aggregate([
            {
                "$match": {
                    "paymentStatus": { "$nin": ["Reverted"] },
                    "fulfilledBy": "MPS0",
                    "orderType": "Wholesale",
                    "stockAllocation": { "$nin": ["NotAllocated"] },
                    "status": { "$in": ["Processing", "Confirmed"] },
                    "subOrders": {
                        "$elemMatch": {
                            "invoiced": false,
                            "status": { "$in": ["Processing", "Confirmed"] },
                            "snapshots": { "$elemMatch": { "snapShotId": snapShotId, "productId": productId, "quantity": { "$gte": count } } }
                        }
                    }
                }
            },
            { "$unwind": "$subOrders" },
            {
                "$match": {
                    "subOrders.invoiced": false,
                    "subOrders.status": { "$in": ["Processing", "Confirmed"] },
                    "subOrders.snapshots": { "$elemMatch": { "snapShotId": snapShotId, "productId": productId, "quantity": { "$gte": count } } }
                }
            },
            { "$unwind": "$subOrders.snapshots" },
            {
                "$match": {
                    "subOrders.snapshots.snapShotId": snapShotId,
                    "subOrders.snapshots.productId": productId,
                    "subOrders.snapshots.quantity": { "$gte": count }
                }
            },
            {
                "$project": {
                    "_id": 1,
                    "subOrders._id": 1,
                    "subOrders.performaInvoiceNo": 1,
                    "subOrders.batchId": 1,
                    "subOrders.snapshots.snapShotId": 1,
                    "subOrders.snapshots.productId": 1,
                    "subOrders.snapshots.quantity": 1
                }
            },
            { "$limit": 1 }
        ]).exec();
    }
}

module.exports = Invoice;